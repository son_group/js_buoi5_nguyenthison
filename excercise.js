function getValue(para){
  var value = document.getElementById(para).value;
  return value;
}

function calculatePoint(){
  var benchmark = getValue("txt-benchmark")*1;
  var studentArea = getValue("student-area")*1;
  var studentObject = getValue("student-object")*1;
  var mathPoint = getValue("txt-mathpoint")*1;
  var physicsPoint = getValue("txt-physics")*1;
  var chemistryPoint = getValue("txt-chemistry")*1;
  var sumPoint = mathPoint + physicsPoint + chemistryPoint +studentArea + studentObject ;
  if(mathPoint==0||physicsPoint==0||chemistryPoint==0){
    document.getElementById("result1").innerHTML = `<span class="text-warning">Bạn không đậu do có điểm liệt.</span><span class="text-success" Hãy cố gắng vào năm sau nhé</span>`
  }else if(sumPoint>=benchmark){
    document.getElementById("result1").innerHTML = `<span class="text-success">Chúc mừng bạn, bạn đã đậu. Tổng điểm của bạn là ${sumPoint}</span>`
  }else{
    document.getElementById("result1").innerHTML = `<span class="text-warning">Bạn không đậu do tổng điểm của bạn là ${sumPoint}. Nhỏ hơn điểm chuẩn</span><span class="text-success" Hãy cố gắng vào năm sau nhé</span>`
  }
}

function calculateMoneyToPay(){
  var family = getValue("txt-familyname");
  var powerNumber = getValue("txt-powernumber")*1;
  var sum = 0;
  if(powerNumber <=50){
    sum +=powerNumber*500;
  }else if(powerNumber <=100){
    sum = 50*500 + (powerNumber-50)*650;
  }else if(powerNumber<=200){
    sum = 50*500 + 50*650 + (powerNumber-100)*850;
  }else if(powerNumber<=350){
    sum = 50*500 + 50*650 + 100*850+(powerNumber-200)*1100;
  }else{
    sum = 50*500 + 50*650 + 100*850 + 150*1100 + (powerNumber-350)*1300;
  }

  console.log({family,powerNumber,sum});
  // document.getElementById("result2").innerHTML="hello";

  document.getElementById("result2").innerHTML = `<span class="text-success">Số tiền mà hộ gia đình ${family} phải trả là ${sum} vnđ</span>`;
}